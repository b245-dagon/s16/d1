// console.log("Hello World");

// [Section] Arithmetic Operators
	// allow us to perfoem mathematical operations between operands (value on either sides of the operator).
	// it returns a numerical value.

let x = 101;
let y = 25;

let sum = x + y ;
console.log("Result of addtion operator: "+sum);

let difference = x - y ;
console.log("Result of subtraction operator: "+difference);

let product = x * y ;
console.log("Result of multiplication operator: "+product);

let quotient = x / y ;
console.log("Result of division operator: "+quotient);

let remaider = x % y ;
console.log("Result of modulo operator: "+remaider);

// Assigment Operator (=)
	// assign the value of the "right operand" to a "variable".

let assignmentNumber = 8;
console.log(assignmentNumber)

// Addition Assignment Operator (+=)
// long method
	// assignmentNumber = assignmentNumber + 2;
// short method
	assignmentNumber +=2;
	console.log("Result of addition assigment operator: "+assignmentNumber);
// Subtraction/multiplication/division assignment
// operator (-= ,*=, /=)

assignmentNumber -=2;
	console.log("Result of subtraction assigment operator: "+assignmentNumber);

assignmentNumber *=2;
	console.log("Result of multiplication assigment operator: "+assignmentNumber);

assignmentNumber /=2;
	console.log("Result of division assigment operator: "+assignmentNumber);

assignmentNumber %=2;
	console.log("Result of remaider assigment operator: "+assignmentNumber);

	// PEMDAS (order of Operations)

	// 
	let mdas = 1 + 2 - 3 * 4 / 5;
	console.log("Result of MDAS operator:"+mdas);


	let pemdas = 1 + (2 - 3) * (4 / 5);
	console.log("Result of PEMDAS operator:"+pemdas);

	pemdas = (1 + (2 - 3)) * (4 / 5);
	console.log("Result of PEMDAS operator:"+pemdas);

	// [SECTION] Increment and Decrement Operator
		// This operators add or subtract values by 1 and reassign the value of the variables where the increment/decrement was applied.
	let z = 1;

	// Increment (++)

	
	let increment = ++z;

	console.log("Result of pre-increment: "+increment);
	console.log("Result of pre-increment: "+z);

	increment = z++;

	console.log("Result of post-increment: "+increment);
	console.log("Result of post-increment: "+z);


	// Decrement (--)
	z = 5;
	let decrement = --z;
	console.log("Result of pre-decrement: "+decrement);
	console.log("Result of pre-decrement: "+z);

	decrement = z--;
	console.log("Result of post-decrement: "+decrement);
	console.log("Result of post-decrement: "+z);



// [SECTION] type Coercion
	// automatic or implicit conversion of value frome one data type to another.
	// This happens when operations are performed on different data types that that would normally not be possible and yield irregular results.
	// "automatic conversion"

	let numA = '10';
	let numB = 12;

	let coercion = numA + numB;
	console.log(coercion);
	console.log(typeof coercion);


	let numC = 16;
	let numD = 14;

	let nonCoercion = numC + numD;
	console.log(nonCoercion);
	console.log(typeof nonCoercion);

	let numE = true + 1;
	console.log(numE);

	let numF = false + 1;
	console.log(numF);

	// [SECTION] Comparison Operator
		// are used to evaluate and compare the left and right operands
		//  it return a boolean value

		let juan = 'juan';


		// Equality operator (==)
		console.log("Equality Operator :");

		console.log(1==1); //true
		console.log(1 == 2); //false
		console.log(1 == '1'); //true
		console.log(false ==0);//true
		console.log('juan' =="juan");//true
		console.log("juan" =="Juan");//false
		console.log(juan =='juan');//true

		

		// Inequality Operators (!=)
		console.log("Inequality Operator:");
		console.log(1 != 1); //false
		console.log(1 != 2); //true
		console.log(1 != '1'); //false
		console.log(false !=0);//false
		console.log('juan' !="juan");//false
		console.log("juan" !="Juan");//true
		console.log(juan !='juan');//false

		// Strict Equality Operator (===)

		/*
			-check whether the operands are equals/have the same content.
			-also COMPARES the data tye of 2 values.


		*/
		console.log("Strict Equality Operator: ");
		console.log(1===1);//true
		console.log(1===2);//false
		console.log(1==='1');//false
		console.log(false ===0);//false
		console.log('juan' === "juan");//true
		console.log("juan"== "Juan");//false
		console.log(juan === 'juan');//true

		// Strict Inequality Operator (!==)
		/*
			- Checks where the operands are not equal/have the different content.
			-also compares the data types of 2 values.
		*/
		console.log("Strict Inequality Operator: ");
		console.log(1!==1);//false
		console.log(1!==2);//true
		console.log(1!=='1');//true
		console.log(false !==0);//true
		console.log('juan' !== "juan");//false
		console.log("juan"!= "Juan");//true
		console.log(juan !== 'juan');//false

		//  [Section] Greater Than and Less Than Operator
			// Some comparison operators check whether one value is greater or less that to the other value.
			// Returns a Boolean value.

		let a = 50;
		let b = 65;

		console.log("Geater than and Less than Operator: ");
		// GT or Greater than (>)
		let isGeaterThan = a > b ;//false
		console.log(isGeaterThan);

		// LT or Less than (<)
		let isLessThan = a < b;
		console.log(isLessThan);

		// GTE or Greater Than or Equal (>=)
		// b = 50;
		let isGTorEqual = a >= b;//false
		console.log(isGTorEqual);

		// LTE or Less Than or Equal (<=)
		let isLTorEqual = a<=b;
		console.log(isLTorEqual)//true

		let numStr = "30";
		console.log(a > numStr);//true

		let strNum = "twenty";
		console.log(b > strNum);//false


	//  [SECTION] Logical Operators
			// to allow for a more specific logical combinations of conditions and evaluations.
			//  It returens a boolean value.

		let isLegalAge = true;
		let isRegistered =  false;

		// Logical And Operator (&&- Double Ampersand)

		let allRequirementsMet = isLegalAge && isRegistered;
		console.log("Result of logical AND Operator:"+allRequirementsMet);//false

		// Logical OR Operator (|| - Double Pipe)
		// returns the opposite value.
		let someRequirementsMet = isLegalAge || isRegistered;
		console.log("Result of logical Or Operator:"+someRequirementsMet);


		//  Logical Not Operator (! -exlamation point)
		console.log("Result of logical NOT Operator:" + !isLegalAge);
		

